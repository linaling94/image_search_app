package com.example.imagesearchapp.model;

import android.util.Log;

import com.example.imagesearchapp.model.network.NaverServiceFactory;
import com.example.imagesearchapp.vo.ImageVO;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

import org.json.JSONArray;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class DataResult implements DataResultInterface {
	@Override
	public void getAllImageList(String keyword, int startNum, Callback<JsonObject> callback) {
		NaverServiceFactory factory = new NaverServiceFactory();
		Call<JsonObject> call = factory.makeNaverService().getSearchResult(keyword, 100, startNum);
		final ArrayList<ImageVO> imageList = new ArrayList<>();

		call.enqueue(callback);
		return;
	}

}
