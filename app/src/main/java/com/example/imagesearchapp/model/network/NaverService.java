package com.example.imagesearchapp.model.network;

import com.example.imagesearchapp.vo.ImageVO;
import com.google.gson.JsonObject;

import org.json.JSONArray;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.Query;

public interface NaverService {

	public static final String URL = "https://openapi.naver.com";

	@Headers({
			"X-Naver-Client-Id: " + "jEcbC2pawOXN84gTyA3P",
			"X-Naver-Client-Secret: " + "B1q1pFzmWP"
	})

	/**
	 * 요청 변수 리스트
	 * query	string	Y	-	검색을 원하는 문자열로서 UTF-8로 인코딩한다.
	 * display	integer	N	10(기본값), 100(최대)	검색 결과 출력 건수 지정
	 * start	integer	N	1(기본값), 1000(최대)	검색 시작 위치로 최대 1000까지 가능
	 * sort	string	N	string	정렬 옵션: sim (유사도순), date (날짜순)
	 * filter	string	N	all (기본값),large, medium, small	사이즈 필터 옵션: all(전체), large(큰 사이즈), medium(중간 사이즈), small(작은 사이즈)
	 */
	@GET("/v1/search/image")
	public Call<JsonObject> getSearchResult(@Query("query") String keyword, @Query("display") int displayCount, @Query("start") int startNum);
}
