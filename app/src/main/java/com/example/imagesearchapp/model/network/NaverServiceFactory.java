package com.example.imagesearchapp.model.network;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class NaverServiceFactory {

	public NaverService makeNaverService() {
		Retrofit retrofit = new Retrofit.Builder()
				.baseUrl(NaverService.URL)
				.addConverterFactory(GsonConverterFactory.create())
				.build();

		NaverService naverService = retrofit.create(NaverService.class);
		return naverService;
	}
}
