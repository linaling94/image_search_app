package com.example.imagesearchapp.model;

import com.example.imagesearchapp.vo.ImageVO;
import com.google.gson.JsonObject;

import java.util.ArrayList;

import retrofit2.Callback;

public interface DataResultInterface {
	public void getAllImageList(String keyword, int startNum, Callback<JsonObject> callback);
}
