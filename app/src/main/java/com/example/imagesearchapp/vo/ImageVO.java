package com.example.imagesearchapp.vo;

/**
 * rss	-	디버그를 쉽게 하고 RSS 리더기만으로 이용할 수 있게 하기 위해 만든 RSS 포맷의 컨테이너이며 그 외의 특별한 의미는 없다.
 * channel	-	검색 결과를 포함하는 컨테이너이다. 이 안에 있는 title, link, description 등의 항목은 참고용으로 무시해도 무방하다.
 * lastBuildDate	datetime	검색 결과를 생성한 시간이다.
 * total	integer	검색 결과 문서의 총 개수를 의미한다.
 * start	integer	검색 결과 문서 중, 문서의 시작점을 의미한다.
 * display	integer	검색된 검색 결과의 개수이다.
 * item/items	-	XML 포멧에서는 item 태그로, JSON 포멧에서는 items 속성으로 표현된다. 개별 검색 결과이며 title, link, thumbnail, sizeheight, sizewidth를 포함한다.
 * title	string	검색 결과 이미지의 제목을 나타낸다.
 * link	string	검색 결과 이미지의 하이퍼텍스트 link를 나타낸다.
 * thumbnail	string	검색 결과 이미지의 썸네일 link를 나타낸다.
 * sizeheight	string	검색 결과 이미지의 썸네일 높이를 나타낸다.
 * sizewidth	string	검색 결과 이미지의 너비를 나타낸다. 단위는 pixel이다.
 */
public class ImageVO {

	private String title;
	private String link;
	private String thumbnail;
	private String sizeheight;
	private String sizewidth;

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getLink() {
		return link;
	}

	public void setLink(String link) {
		this.link = link;
	}

	public String getThumbnail() {
		return thumbnail;
	}

	public void setThumbnail(String thumbnail) {
		this.thumbnail = thumbnail;
	}

	public String getSizeheight() {
		return sizeheight;
	}

	public void setSizeheight(String sizeheight) {
		this.sizeheight = sizeheight;
	}

	public String getSizewidth() {
		return sizewidth;
	}

	public void setSizewidth(String sizewidth) {
		this.sizewidth = sizewidth;
	}
}
