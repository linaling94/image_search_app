package com.example.imagesearchapp.view;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.imagesearchapp.R;

public class SearchViewActivity extends AppCompatActivity {

	public static final int REQ_SEARCH_VIEW = 1111;
	private EditText mEtKeyword;
	private Button mBtnSearch;
	public static final String KEY_WORD = "keyword";

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_search_view);

		initView();
	}

	private void initView() {
		mEtKeyword = findViewById(R.id.et_keyword);
		mBtnSearch = findViewById(R.id.btn_search);
		mBtnSearch.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				// go next view
				Intent intent = new Intent(SearchViewActivity.this, ResultViewActivity.class);
				intent.putExtra(KEY_WORD, mEtKeyword.getText());
				startActivityForResult(intent, REQ_SEARCH_VIEW);
			}
		});
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
		if(requestCode == REQ_SEARCH_VIEW) {
			if(resultCode == RESULT_OK) {
				if(data.getStringExtra("message") != null) {
					Toast.makeText(this, data.getStringExtra("message"), Toast.LENGTH_SHORT).show();
				}
			}
		}
	}

	@Override
	protected void onResume() {
		super.onResume();
		mEtKeyword.setText("");
	}
}
