package com.example.imagesearchapp.view;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatRadioButton;
import androidx.core.widget.ContentLoadingProgressBar;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager.widget.PagerAdapter;
import androidx.viewpager.widget.ViewPager;

import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.example.imagesearchapp.R;
import com.example.imagesearchapp.controller.DataSearch;
import com.example.imagesearchapp.controller.DataSearchInterface;
import com.example.imagesearchapp.vo.ImageVO;

import java.util.ArrayList;
import java.util.List;

public class ResultViewActivity extends AppCompatActivity implements ResultViewInterface {

	public static final String IMAGE_LINK = "imageLink";

	private ContentLoadingProgressBar mProgressBar;
	private ImageView mBtnGoBack;
	private TextView mTvKeyword;
	private RadioGroup mRgListStyle;
	private AppCompatRadioButton mRdoList;
	private AppCompatRadioButton mRdoTable;
	private RecyclerView mRvResultList;
	private ResultAdapter mResultAdapter;

	private ArrayList<ImageVO> mResultList = new ArrayList<>();

	private DataSearchInterface mSearchController;

	private boolean isGrid = false;
	private int startNum = 1;

	public ResultViewActivity() {
		this.mSearchController = new DataSearch(this);
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_result_view);

		initView();
		setContents();
	}

	private void initView() {
		mProgressBar = findViewById(R.id.progress_bar);
		mBtnGoBack = findViewById(R.id.btn_go_back);
		mBtnGoBack.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				onBackPressed();
			}
		});
		mTvKeyword = findViewById(R.id.tv_keyword);

		mRvResultList = findViewById(R.id.rv_result_list);
		mRvResultList.setLayoutManager(new LinearLayoutManager(this));
		mResultAdapter = new ResultAdapter(mResultList);
		mRvResultList.setAdapter(mResultAdapter);
		mRvResultList.addOnScrollListener(new RecyclerView.OnScrollListener() {
			@Override
			public void onScrollStateChanged(@NonNull RecyclerView recyclerView, int newState) {
				super.onScrollStateChanged(recyclerView, newState);
			}

			@Override
			public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
				super.onScrolled(recyclerView, dx, dy);

				if(!mRvResultList.canScrollVertically(1)) {
					startNum += 100;
					mSearchController.doSearch(mTvKeyword.getText(), startNum);
				}
			}
		});

		mRgListStyle = findViewById(R.id.rg_list_style);
		mRgListStyle.getChildCount();
		mRgListStyle.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
			@Override
			public void onCheckedChanged(RadioGroup radioGroup, int i) {
				if(i == mRdoList.getId()) {
					// List
					isGrid = false;
					mRvResultList.setLayoutManager(new LinearLayoutManager(getApplicationContext()));
				} else if(i == mRdoTable.getId()) {
					// Grid
					isGrid = true;
					mRvResultList.setLayoutManager(new GridLayoutManager(getApplicationContext(), 3));
				}
			}
		});
		mRdoList = findViewById(R.id.rdo_list);
		mRdoTable = findViewById(R.id.rdo_table);
	}

	private void setContents() {
		CharSequence keyword = getIntent().getCharSequenceExtra(SearchViewActivity.KEY_WORD);
//		String keyword = getIntent().getStringExtra(SearchViewActivity.KEY_WORD);
		mTvKeyword.setText(keyword);
		mRdoList.setChecked(true);

		// search controller method call
		mResultList = new ArrayList<>();
		mSearchController.doSearch(keyword, startNum);
	}

	public class ResultAdapter extends RecyclerView.Adapter<ResultAdapter.ItemHolder> {

		private ArrayList<ImageVO> imageList = new ArrayList<>();

		class ItemHolder extends RecyclerView.ViewHolder {

			private TextView tvImageName;
			private TextView tvImageLink;
			private ImageView ivThumbnail;
			private LinearLayout llText;

			public ItemHolder(@NonNull View itemView) {
				super(itemView);

				tvImageName = itemView.findViewById(R.id.tv_image_name);
				tvImageLink = itemView.findViewById(R.id.tv_image_link);
				ivThumbnail = itemView.findViewById(R.id.iv_thumbnail);
				llText = itemView.findViewById(R.id.ll_text);
			}
		}

		public ResultAdapter(ArrayList<ImageVO> imageList) {
			this.imageList = imageList;
		}


		@NonNull
		@Override
		public ItemHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
			Context context = parent.getContext();
			LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			View view = inflater.inflate(R.layout.layout_image_item, parent, false);
			ItemHolder itemHolder = new ItemHolder(view);
			return itemHolder;
		}

		@Override
		public void onBindViewHolder(@NonNull ItemHolder holder, int position) {
			if(isGrid) {
				holder.llText.setVisibility(View.GONE);
			} else {
				holder.llText.setVisibility(View.VISIBLE);
			}
			final ImageVO image = imageList.get(position);
			holder.tvImageName.setText(image.getTitle());
			holder.tvImageLink.setText(image.getLink());
			Glide.with(getApplicationContext()).load(image.getThumbnail()).into(holder.ivThumbnail);

			holder.itemView.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View view) {
					Intent intent = new Intent(ResultViewActivity.this, DetailViewActivity.class);
					intent.putExtra(SearchViewActivity.KEY_WORD, mTvKeyword.getText());
					intent.putExtra(IMAGE_LINK, image.getLink());
					startActivity(intent);
				}
			});
		}

		@Override
		public int getItemCount() {
			return imageList.size();
		}

		public void changeList(ArrayList<ImageVO> newImageList) {
			imageList = newImageList;
			notifyDataSetChanged();
		}
	}

	/**
	 * controller에서 호출하는 결과 보여주는 메소드
	 * @param imageList
	 */
	@Override
	public void drawImageData(ArrayList<ImageVO> imageList) {
		mResultList.addAll(imageList);
		mResultAdapter.changeList(mResultList);
	}

	@Override
	public void setStartNum(int startNum) {
		this.startNum = startNum;
	}

	@Override
	public void showLoadingBar() {
		if(!isLoadingBarShow()) {
			mProgressBar.setVisibility(View.VISIBLE);
		}
	}

	@Override
	public void hideLoadingBar() {
		if(isLoadingBarShow()) {
			mProgressBar.setVisibility(View.GONE);
		}
	}

	@Override
	public void showMessageForEmptyMessage(String message) {
		Intent intent = getIntent();
		intent.putExtra("message", message);
		setResult(RESULT_OK, intent);
		finish();
	}

	@Override
	public CharSequence getKeyword() {
		return mTvKeyword.getText();
	}

	public boolean isLoadingBarShow() {
		return mProgressBar.getVisibility() == View.VISIBLE;
	}

}
