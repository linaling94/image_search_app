package com.example.imagesearchapp.view;

import androidx.appcompat.app.AppCompatActivity;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.example.imagesearchapp.R;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

public class DetailViewActivity extends AppCompatActivity {

	private Bitmap mBitmap;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_detail_view);

		setContents();
		setDetailImage();
	}

	public void setContents() {
		ImageView btnGoBack = findViewById(R.id.btn_go_back);
		btnGoBack.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				onBackPressed();
			}
		});
		TextView tvKeyword = findViewById(R.id.tv_keyword);
		tvKeyword.setText(getIntent().getStringExtra(SearchViewActivity.KEY_WORD));
	}

	public void setDetailImage() {

		Thread thread = new Thread() {
			@Override
			public void run() {
				try {
					URL url = new URL(getIntent().getStringExtra(ResultViewActivity.IMAGE_LINK));
					try {
						HttpURLConnection conn = (HttpURLConnection) url.openConnection();
						conn.setDoInput(true);
						conn.connect();

						InputStream is = conn.getInputStream();
						mBitmap = BitmapFactory.decodeStream(is);
					} catch (IOException e) {
					}
				} catch (MalformedURLException e) {
				}
			}
		};
		thread.start();

		try {
			thread.join();
			ImageView ivImage = findViewById(R.id.iv_image);
			ivImage.setImageBitmap(mBitmap);

		} catch (InterruptedException e) {
		}
	}

}
