package com.example.imagesearchapp.view;

import android.view.View;

import com.example.imagesearchapp.vo.ImageVO;

import java.util.ArrayList;

public interface ResultViewInterface {
	public void drawImageData(ArrayList<ImageVO> imageList);
	public void setStartNum(int startNum);
	public void showLoadingBar();
	public void hideLoadingBar();
	public void showMessageForEmptyMessage(String message);
	public CharSequence getKeyword();
}
