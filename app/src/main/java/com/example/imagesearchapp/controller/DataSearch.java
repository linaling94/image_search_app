package com.example.imagesearchapp.controller;

import android.util.Log;

import com.example.imagesearchapp.model.DataResult;
import com.example.imagesearchapp.model.DataResultInterface;
import com.example.imagesearchapp.view.ResultViewActivity;
import com.example.imagesearchapp.view.ResultViewInterface;
import com.example.imagesearchapp.vo.ImageVO;
import com.google.gson.JsonObject;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class DataSearch implements DataSearchInterface{

	private DataResultInterface mDataResult;
	private ResultViewInterface mResultViewActivity;

	public DataSearch(ResultViewActivity resultViewActivity) {
		mDataResult = new DataResult();
		this.mResultViewActivity = resultViewActivity;
	}

	@Override
	public void doSearch(CharSequence keyword, int startNum) {

		if(keyword == null || keyword.length() < 1) {
			mResultViewActivity.showMessageForEmptyMessage("검색어를 입력하세요");
			return;
		}
		final String keywordString = keyword.toString();
		// search ..
		mResultViewActivity.showLoadingBar();
		mDataResult.getAllImageList(keywordString, startNum, new Callback<JsonObject>() {
			@Override
			public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
				mResultViewActivity.hideLoadingBar();
				if(response.body() == null) {
					mResultViewActivity.showMessageForEmptyMessage("결과가 없습니다.");
					return;
				}
				ArrayList<ImageVO> imageList = makeImageDataList(keywordString, response.body());
				if(imageList.size() > 0) {
					mResultViewActivity.drawImageData(imageList);
				}
			}

			@Override
			public void onFailure(Call<JsonObject> call, Throwable t) {
				mResultViewActivity.hideLoadingBar();
				Log.d("DATA", "ERROR");
			}
		});
	}

	@Override
	public ArrayList<ImageVO> makeImageDataList(String keyword, JsonObject resultJson) {

		ArrayList<ImageVO> imageList = new ArrayList<>();
		int displayCount = resultJson.get("display").getAsInt();
		if(displayCount > 0) {
			for(int i = 0; i < displayCount; i++) {
				ImageVO imageVO = new ImageVO();
				JsonObject object = (JsonObject) resultJson.getAsJsonArray("items").get(i);
				imageVO.setTitle(object.get("title").getAsString());
				imageVO.setThumbnail(object.get("thumbnail").getAsString());
				imageVO.setLink(object.get("link").getAsString());
				imageVO.setSizeheight(object.get("sizeheight").getAsString());
				imageVO.setSizewidth(object.get("sizewidth").getAsString());

				imageList.add(imageVO);
			}
		}
		return imageList;
	}
}
