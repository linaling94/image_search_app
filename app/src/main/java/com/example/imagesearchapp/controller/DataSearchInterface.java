package com.example.imagesearchapp.controller;

import com.example.imagesearchapp.vo.ImageVO;
import com.google.gson.JsonObject;

import java.util.ArrayList;

public interface DataSearchInterface {
	public void doSearch(CharSequence keyword, int startNum);
	public ArrayList<ImageVO> makeImageDataList(String keyword, JsonObject resultJson);
}
