package com.example.imagesearchapp;

import android.util.MonthDisplayHelper;

import com.example.imagesearchapp.controller.DataSearch;
import com.example.imagesearchapp.controller.DataSearchInterface;
import com.example.imagesearchapp.model.DataResult;
import com.example.imagesearchapp.model.DataResultInterface;
import com.example.imagesearchapp.view.ResultViewActivity;
import com.example.imagesearchapp.view.ResultViewInterface;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;

import javax.security.auth.callback.Callback;

public class SearchControllerTest {

	ResultViewInterface mockResultView;
	DataResultInterface mockDataResult;

	DataSearchInterface dataSearch;

	@Before
	public void setup() {
		mockResultView = Mockito.mock(ResultViewActivity.class);
		mockDataResult = Mockito.mock(DataResult.class);
		dataSearch = new DataSearch((ResultViewActivity) mockResultView);
	}

	/**
	 * 아무것도 입력 안함
	 */
	@Test
	public void search_fail_empty_input() {
		// given 아무것도 입력하지 않는다.
		Mockito.when(mockResultView.getKeyword()).thenReturn("");
		// when 검색 버튼을 클릭한다.
		dataSearch.doSearch(mockResultView.getKeyword(),1);
		// 검증 실패 & 토스트 메세지를 보여준다.
		Mockito.verify(mockResultView).showMessageForEmptyMessage("검색어를 입력하세요");
	}

	@Test
	public void search_success() {
		// given 문자를 입력한다.
		Mockito.when(mockResultView.getKeyword()).thenReturn("옥수수");
		// when 검색 버튼을 클릭한다.
		dataSearch.doSearch(mockResultView.getKeyword(), 1);

		// 검색 문자열을 전달하여 검색한다.
		CharSequence text = mockResultView.getKeyword();

	}
}
